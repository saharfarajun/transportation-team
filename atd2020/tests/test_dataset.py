from pathlib import Path
import unittest

import atd2020
from atd2020.detector import BaselineDetector
from atd2020.detrend import detrend
from atd2020.utilities import read_data
from atd2020.metrics import metrics

# TODOs
# - Verify `Fraction_Observed` matches `Observed`
# - In city1, ensure only missing values are daylight savings time
# - test results.csv

DF_COLUMNS = [
    "Timestamp",
    "ID",
    "Observed",
    "TotalFlow",
    "Year",
    "Month",
    "Day",
    "Hour",
    "Weekday",
    "Latitude",
    "Longitude",
    "Fraction_Observed",
    "Observed",
]


def assertGeoBounded(test_case, df):
    geo_cols = ["Latitude", "Longitude"]
    test_case.assertGreaterEqual(df[geo_cols].min().min(), 0)
    test_case.assertLessEqual(df[geo_cols].max().max(), 1)


def assertGeoFillsUnitSquare(test_case, df):
    geo_cols = ["Latitude", "Longitude"]
    test_case.assertEqual(df[geo_cols].min().min(), 0)
    test_case.assertEqual(df[geo_cols].max().max(), 1)


def assertYearsMatch(test_case, df, years):
    test_case.assertEqual(df["Year"].min(), years[0], "Min year does not match")
    test_case.assertEqual(df["Year"].max(), years[-1], "Max year does not match")


def assertColumnsMatch(test_case, df, columns):
    test_case.assertEqual(set(df.columns), set(columns), "Columns don't match")


def assertNoNan(test_case, df):
    test_case.assertEqual(len(df.dropna()), len(df), "NaN in dataset")


def assertSensorCount(test_case, df, count):
    test_case.assertEqual(df["ID"].nunique(), count, "Too many sensors")


def assertValidCity(test_case, df, columns, years, count):
    assertColumnsMatch(test_case, df, columns)
    assertGeoBounded(test_case, df)
    assertYearsMatch(test_case, df, years)
    assertSensorCount(test_case, df, count)
    assertNoNan(test_case, df)


class TestDataset1(unittest.TestCase):
    def setUp(self):
        self.df = read_data(Path("data") / "City1.parquet.brotli")

    def test_city1(self):
        df = self.df
        years = [2016, 2017]
        columns = DF_COLUMNS + ["Anomaly"]
        n_sensors = 500
        n_obs = df.groupby("ID")["ID"].count()

        self.assertTrue((n_obs == n_obs.iloc[0]).all(), "Unequal # of obs/ID")

        # Test on first 100 IDs, because gitlab runner fails from lack of mem
        df_first_100 = df.loc[df["ID"].isin(df["ID"].unique()[:100])]
        df_metrics = metrics(
            df_first_100, BaselineDetector().fit_predict(detrend(df_first_100))
        )
        self.assertEqual(
            df_metrics.loc["overall", "Accuracy"],
            1.0,
            "Imperfect accuracy on full dataset",
        )
        assertGeoFillsUnitSquare(self, df)
        assertValidCity(self, df, columns, years, n_sensors)


class TestDataset2(unittest.TestCase):
    def setUp(self):
        self.df = read_data(Path("data") / "City2_downsampled.parquet.brotli")

    def test_city2(self):
        df = self.df
        years = [2014, 2015]
        columns = DF_COLUMNS
        n_sensors = 500

        assertValidCity(self, df, columns, years, n_sensors)


if __name__ == "__main__":
    unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output="test-reports"),
        failfast=False,
        buffer=False,
        catchbreak=False,
    )
