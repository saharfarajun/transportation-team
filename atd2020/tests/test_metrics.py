import subprocess
import unittest

import matplotlib as mpl
import numpy as np
import pandas as pd

import atd2020

METRIC_COLS = ["True Positive", "False Positive", "True Negative", "False Negative"]


def make_data(start="2020/1/1 00:00:00"):
    n_obs = 5
    ids = np.repeat([0, 1, 2], n_obs)
    frac_obs = np.repeat([0.1, 0.1, 0.2], n_obs)
    timestamp_id = pd.date_range(start, periods=n_obs, freq="h").to_list()
    timestamp = np.tile(timestamp_id, 3)
    pred0 = [True, False, True, False, True]
    true0 = [False, True, True, False, True]
    pred1 = [False, False, False, False, False]
    true1 = [False, False, False, False, False]
    pred2 = [True, True, True, True, True]
    true2 = [True, True, True, True, True]
    pred = np.hstack([pred0, pred1, pred2])
    true = np.hstack([true0, true1, true2])

    df_true = pd.DataFrame(
        {
            "ID": ids,
            "Fraction_Observed": frac_obs,
            "Timestamp": timestamp,
            "Anomaly": true,
        }
    )
    df_pred = pd.DataFrame({"ID": ids, "Timestamp": timestamp, "Anomaly": pred})

    tp_fp_tn_fn = lambda tp, fp, tn, fn: {
        "True Positive": tp,
        "False Positive": fp,
        "True Negative": tn,
        "False Negative": fn,
    }

    expected = {
        ("overall", "overall"): tp_fp_tn_fn(7, 1, 6, 1),
        (0.1, "overall"): tp_fp_tn_fn(2, 1, 6, 1),
        (0.1, 0): tp_fp_tn_fn(2, 1, 1, 1),
        (0.1, 1): tp_fp_tn_fn(0, 0, 5, 0),
        (0.2, "overall"): tp_fp_tn_fn(5, 0, 0, 0),
        (0.2, 2): tp_fp_tn_fn(5, 0, 0, 0),
    }

    return (
        df_true,
        df_pred,
        pd.DataFrame.from_dict(expected, orient="index")
        .rename_axis(["Fraction_Observed", "ID"])
        .convert_dtypes(),
    )


class TestMetrics(unittest.TestCase):
    def setUp(self):
        self.df_true, self.df_pred, self.df_expected = make_data()

    def test_groupby_none(self):
        actual = atd2020.metrics.metrics(self.df_true, self.df_pred, groupby=None)
        expected = self.df_expected.loc["overall"][METRIC_COLS].rename_axis(None)
        pd.testing.assert_frame_equal(expected[METRIC_COLS], actual[METRIC_COLS])

    def test_groupby_fraction_observed(self):
        actual = atd2020.metrics.metrics(
            self.df_true, self.df_pred, groupby="Fraction_Observed"
        )
        expected = self.df_expected.xs("overall", level="ID")
        pd.testing.assert_frame_equal(expected[METRIC_COLS], actual[METRIC_COLS])

    def test_groupby_fraction_observed_id(self):
        actual = atd2020.metrics.metrics(
            self.df_true, self.df_pred, groupby=["Fraction_Observed", "ID"]
        )
        expected = self.df_expected.drop(index=[(0.1, "overall"), (0.2, "overall")])
        pd.testing.assert_frame_equal(expected[METRIC_COLS], actual[METRIC_COLS])

    def test_output_df_false(self):
        actual = atd2020.metrics.metrics(self.df_true, self.df_pred, output_df=False)
        self.assertIsInstance(
            actual, str, "metrics(..., output_df=False did not output str."
        )

    def test_output_flat_true(self):
        actual = atd2020.metrics.metrics(self.df_true, self.df_pred, output_flat=True)
        self.assertEqual(actual.shape[0], 1, "Incorrect number of rows")

    def test_flatten_groupy_of_type_str(self):
        actual = atd2020.metrics.metrics(
            self.df_true, self.df_pred, groupby="Fraction_Observed", output_flat=True
        )
        self.assertEqual(len(actual), 1, "Incorrect number of rows")

    def test_flatten_groupy_of_type_list_len_1(self):
        actual = atd2020.metrics.metrics(
            self.df_true, self.df_pred, groupby=["Fraction_Observed"], output_flat=True
        )
        self.assertEqual(len(actual), 1, "Incorrect number of rows")

    def test_flatten_raises_error_for_multiindex(self):
        with self.assertRaises(TypeError, msg="Did not raise TypeError"):
            atd2020.metrics.metrics(
                self.df_true,
                self.df_pred,
                groupby=["Fraction_Observed", "ID"],
                output_flat=True,
            )

    def test_metrics_cli_no_groupby(self):
        cmd = "python -m atd2020.metrics -p results/City1.csv -t data/City1.parquet.brotli"
        process_output = subprocess.run(
            cmd, shell=True, capture_output=True, check=True
        )
        self.assertEqual(process_output.returncode, 0)

    def test_metrics_cli_groupby_one(self):
        cmd = "python -m atd2020.metrics -p results/City1.csv -t data/City1.parquet.brotli -g Fraction_Observed"
        process_output = subprocess.run(
            cmd, shell=True, capture_output=True, check=True
        )
        self.assertEqual(process_output.returncode, 0)

    def test_metrics_cli_groupby_list(self):
        cmd = "python -m atd2020.metrics -p results/City1.csv -t data/City1.parquet.brotli -g Fraction_Observed Year"
        process_output = subprocess.run(
            cmd, shell=True, capture_output=True, check=True
        )
        self.assertEqual(process_output.returncode, 0)

    def test_metrics_plot_numeric_x_false(self):
        actual = atd2020.metrics.metrics(
            self.df_true, self.df_pred, groupby=["Fraction_Observed"], output_flat=True
        )
        ax = atd2020.metrics.plot(actual, metric="F1", numeric_x=False)
        self.assertIsInstance(ax, mpl.axes.Axes)

    def test_metrics_plot_numeric_x_True(self):
        actual = atd2020.metrics.metrics(
            self.df_true, self.df_pred, groupby=["Fraction_Observed"], output_flat=True
        )
        ax = atd2020.metrics.plot(actual, metric="F1", numeric_x=True)
        # TODO assert labels are numeric rather than categorical
        self.assertIsInstance(ax, mpl.axes.Axes)


if __name__ == "__main__":
    unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output="test-reports"),
        failfast=False,
        buffer=False,
        catchbreak=False,
    )
