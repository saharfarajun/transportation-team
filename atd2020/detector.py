from matplotlib import pyplot as plt
import numpy as np
import scipy.stats


class BaselineDetector(object):
    """Anomaly detector used for ground truthing and baseline comparison.

    Parameters
    ----------
    groupby : iterable, default=("ID", "Weekday", "Hour")
        List of column names by which observations are grouped.
    """

    def __init__(self, groupby=("ID", "Weekday", "Hour")):
        self.groupby = np.asarray(groupby).tolist()
        self.group_stats = None
        self.fallback_lower_thresh = None
        self.fallback_upper_thresh = None

    def fit(self, df, n_std=3):
        """Fit anomaly thresholds for groups induced by self.groupby.

        Parameters
        ----------
        df : pandas.DataFrame
            A dataframe containing all fields in self.groupby, "Timestamp",
            and "TotalFlow".
        n_std: float, default=3
            Number of standard deviations outside which TotalFlow observations
            will be considered an anomaly.

        Returns
        -------
        self
        """

        self.group_stats = df.groupby(self.groupby)["TotalFlow"].agg(
            ["mean", "std", "count"]
        )
        halfwidth = n_std * self.group_stats["std"]
        self.group_stats["lower_thresh"] = self.group_stats["mean"] - halfwidth
        self.group_stats["upper_thresh"] = self.group_stats["mean"] + halfwidth

        lower, upper = scipy.stats.norm.cdf((-n_std, n_std))
        self.fallback_lower_thresh = df["TotalFlow"].quantile(lower)
        self.fallback_upper_thresh = df["TotalFlow"].quantile(upper)
        return self

    def predict(self, df):
        """Apply detector's anomaly detection rule to observations in df.

        If there are no group_stats for an observation, we use a fallback rule.
        We set the thresholds to the lower/upper quantiles of TotalFlow where
        the quantiles are set by evaluating the CDF of the standard normal at
        -n_std and n_std respectively (from self.fit). We indicate points
        requiring use of the fallback rule in the "Fallback" column.

        Parameters
        ----------
        df : pandas.DataFrame
            A dataframe containing all fields in self.groupby, "Timestamp",
            and "TotalFlow".

        Returns
        -------
        pandas.Dataframe
            Input df with predictions written to column="Anomaly", forecast
            written to columns=["lower_thresh", "mean", "upper_thresh"], and
            whether an observation required use of the fallback rule in the
            column="Fallback".
        """

        df_out = df.merge(self.group_stats, on=self.groupby, how="left")

        fallback = df_out["lower_thresh"].isna()
        df_out.loc[fallback, "lower_thresh"] = self.fallback_lower_thresh
        df_out.loc[fallback, "upper_thresh"] = self.fallback_upper_thresh
        df_out["Fallback"] = fallback

        df_out["Anomaly"] = ~df_out["TotalFlow"].between(
            df_out["lower_thresh"], df_out["upper_thresh"]
        )

        return df_out

    def fit_predict(self, df, n_std=3):
        """Run fit and predict methods in sequence.

        Parameters
        ----------
        df : pandas.DataFrame
            Contains all fields in self.groupby, "Timestamp", and "TotalFlow".
        n_std: float, default=3
            Number of standard deviations outside which TotalFlow observations
            will be considered an anomaly.

        Returns
        -------
        pandas.Dataframe
            Input df with predictions written to column="Anomaly".
        """

        return self.fit(df, n_std=n_std).predict(df)


def plot(df, ax=None):
    """Plot the detector's forecast and predicted anomalies.

    Parameters
    ----------
    df : pandas.DataFrame
        Contains columns=["Timestamp", "lower_thresh", mean", upper_thresh",
        "TotalFlow", "Anomaly"]

    ax : matplotlib.axes.Axes, default=None
        Axes upon which to plot. If None, an axes is created.

    Returns
    -------
    matplotlib.axes.Axes
        Axes with detector forecast and predicted anomalies plotted.
            * x-axis Timestamp
            * y-axis TotalFlow
            * Red `x` markers for anomalies
            * Black `.` markers for normal observations
            * Blue line for mean predicted value
            * Gray region between lower/upper anomaly thresholds
    """

    if ax is None:
        _, ax = plt.subplots()
    marker_kwargs = {"ax": ax, "linestyle": "none"}
    line_kwargs = {"ax": ax, "linestyle": "dashed", "linewidth": 0.2}
    df = df.set_index("Timestamp", drop=False)
    df["mean"].plot(color="blue", **line_kwargs)
    ax.fill_between(
        df["Timestamp"],
        df["lower_thresh"],
        df["upper_thresh"],
        alpha=0.2,
        interpolate=True,
    )
    df["TotalFlow"].loc[df["Anomaly"]].plot(
        color="red", marker="x", markersize=6, **marker_kwargs
    )
    df["TotalFlow"].loc[~df["Anomaly"]].plot(
        color="black", marker=".", markersize=2, **marker_kwargs
    )
    return ax
