#!/usr/bin/env python

from setuptools import setup, find_packages


setup(
    name="atd2020",
    version="0.1.0",
    description="Baseline detector, metrics, and utilities for ATD2020 challenge.",
    author="Douglas Mercer",
    author_email="dgm161@arl.psu.edu",
    python_requires=">=3.5",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "numpy",
        "matplotlib",
        "pandas",
        "pyarrow",
        "seaborn",
        "scikit-learn",
        "statsmodels",
    ],
)
