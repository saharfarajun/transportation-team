# This module will just contain the background methods for running the
# classifiers in the notebooks.
import numpy as np
import pandas as pd
import collections
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.preprocessing import normalize, StandardScaler
from sklearn.cluster import KMeans


# Shamelessly stolen from Calvin's feature engineering stuff. The man is smart.
weekdays = {
    "Monday": 1,
    "Tuesday": 2,
    "Wednesday": 3,
    "Thursday": 4,
    "Friday": 5,
    "Saturday": 6,
    "Sunday": 7,
}


def make_features_label(
    data,
    drop=[
        "Timestamp",
        "Fraction_Observed",
        "Observed",
        "Anomaly",
        "ID",
        "Latitude",
        "Longitude",
        "TotalFlow",
    ],
):
    """This is the main method to run to convert the raw data into features and
        a label. The data given should be the pandas city1 data, although
        minor modifications of the data is reasonable. The optional "drop"
        parameter is just to specify which columns should be dropped."""
    # data["Seconds_since_epoch"] = data["Timestamp"].apply(lambda x: dt2epoch(x))

    # cycical nature captured by creating two dimensions (https://ianlondon.github.io/blog/encoding-cyclical-features-24hour-time/)
    # Represent data['Hour'] by transforming it into sin/cos representations
    data["sin_hour"] = np.sin(2 * np.pi * data.Hour / 24) # I'm not convinced that circular data is important for decision trees
    data["cos_hour"] = np.cos(2 * np.pi * data.Hour / 24)
    if "Weekday" in data:
        data["Weekday"] = data["Weekday"].apply(
            lambda day, weekdays=weekdays: weekdays.get(day, 0)
        )
    data["sin_weekday"] = np.sin(2 * np.pi * data.Weekday / 7) # I'm not convinced that the circular data is important for decision trees
    data["cos_weekday"] = np.cos(2 * np.pi * data.Weekday / 7)

    # Scaling the data to bring all the attributes to a comparable level
    scaler = StandardScaler()
    # data
    X_scaled = scaler.fit_transform(data)

    # Normalizing the data so that the data
    # approximately follows a Gaussian distribution
    X_normalized = normalize(X_scaled)

    # Converting the numpy array into a pandas DataFrame
    X_normalized = pd.DataFrame(X_normalized)

    # Renaming the columns
    X_normalized.columns = data.columns



    kmeans = KMeans(n_clusters=20, random_state=0).fit(X_normalized)

    X_normalized['kmeans_cluster'] = kmeans.predict(X_normalized)


    # Make the label and the features.
    label = pd.DataFrame(data["Anomaly"])
    features = X_normalized

    # Normalize lat and long if they aren't already. This should have no effect if the lat and long are already normalized.
    if "Latitude" in features.columns:
        features["Latitude"] = (features["Latitude"] - features["Latitude"].mean()) / features["Latitude"].std()

    if "Longitude" in features.columns:
        features["Longitude"] = (features["Longitude"] - features["Longitude"].mean()) / features["Longitude"].std()


    return features, label

# Graphs the predictions. Duh.
def graph_predictions(spatial_data, features, labels, model, metric, fig, ax, title, classification="True"):
    """Graphs the predictions. Title is the title of the figure. Returns the mappable image (ie the result of ax.imshow()."""
    class_report = collections.defaultdict(dict)
    score = collections.defaultdict(dict)

    for x in range(len(features)):
        for y in range(len(features[x])):
            if isinstance(features[x][y], int) or len(features[x][y]) == 0 or model == None:
                continue

            prediction = model.predict(features[x][y])
            reality = labels[x][y]["Anomaly"].tolist()

            class_report[x][y] = classification_report(
                prediction, reality, output_dict=True
            )

    score = np.empty(0)
    for x in range(len(features)):
        for y in range(len(features[x])):
            if isinstance(features[x][y], int) or len(features[x][y]) == 0 or model == None or (True not in prediction):
                score = np.append(score, 0)
                ax.text(x, 2 - y, "N/A", ha="center", va="center", color="w")
                continue

            report = class_report[x][y][classification][metric]
            score = np.append(score, report)
            ax.text(x, 2 - y, "{:3.2f}".format(report), ha="center", va="center", color="w")
    score.shape = 3, 3
    score = np.rot90(score)  # Because it appends them with

    im = ax.imshow(score)
    im.set_clim(vmin=0, vmax=1)
    ax.set_xticks([0, 1, 2])
    ax.set_yticks([0, 1, 2])
    ax.set_yticklabels([2, 1, 0])
    ax.set_title(title)

    return im

def gen_f1scores(models, features, labels, include_training=False):
    """Takes in the models, features and labels, and makes predictions for each model on the data from all the other
    sectors. Returns a dictionary with the same dimensions as models. Each value corresponds to that model's f1-score on
    all the other sectors."""
    f1score = collections.defaultdict(dict)
    count = 0
    total = 0.0

    for i in range(3):
        for j in range(3):
            if models[i][j] is None:
                continue

            f1score[i][j] = 0.0
            temp = 0

            for x in range(3):
                for y in range(3):
                    if isinstance(features[x][y], int) or len(features[x][y]) == 0:
                        continue

                    if not include_training and i == x and j == y:
                        continue  # No testing on the training data!

                    prediction = models[i][j].predict(features[x][y])
                    reality = labels[x][y]["Anomaly"].tolist()

                    class_report = classification_report(
                        prediction, reality, output_dict=True
                    )

                    if "True" in class_report:
                        f1score[i][j] += class_report["True"]["f1-score"] * len(features[x][y])
                    else:
                        f1score[i][j] += 0 # If you're not going to make any predictions, you don't get a good f1-score
                    temp += len(features[x][y])

            f1score[i][j] /= temp
            total += f1score[i][j]
            count += 1

    return f1score

    #fig.colorbar(im, ax=ax)


def print_f1s(f1score):
    """Displays the average f1-scores for a group of models' predictions on
    testing data, as well as which model did best overall. Does not return
    anything."""
    highest = h_row = h_col = 0
    lowest = l_row = l_col = 1

    total = 0.0
    count = 0
    for row, values in f1score.items():
        for col, score in values.items():
            if score > highest:
                highest = score
                h_row = row
                h_col = col

            if score < lowest:
                lowest = score
                l_row = row
                l_col = col

            total += score
            count += 1

    print("Highest: " + str("{:4.3f}".format(highest)), end=". ")
    print("Sector: " + str(h_row) + ", " + str(h_col) + ")")
    print("Lowest: " + str("{:4.3f}".format(lowest)), end=". ")
    print("Sector: (" + str(l_row) + ", " + str(l_col) + ")")
    print("Average: " + str("{:4.3f}".format(total / count)))

def make_predictions(model, features, data, train_x=-1, train_y=-1, as_type=bool):
    """Takes a single model and uses it to predict anomalies on all the data. This will include predictions made on
    its own training data. If you don't want predictions on the training data, set train_x and train_y to the
    which contains the training data. The predictions are returned as a column in a (modified) data.
    
    By default, the column returned will be a boolean - True for a predicted anomaly and False for not. If you would
    like to see how sure the model is that a sample is an anomaly, set as_type=float. This will return the 
    probability for each sample to be an anomaly."""
    
    data["Pred_Anomaly"] = None
    for x, row in features.items():
        for y, region in row.items():
            if x == train_x & y == train_y:
                continue

            if isinstance(region, int) or len(region) == 0:
                continue

            if as_type == bool:
                data.loc[data.index.isin(region.index), ["Pred_Anomaly"]] = model.predict(region)
            
            elif as_type == float:
                data.loc[data.index.isin(region.index), ["Pred_Anomaly"]] = [t for f,t in model.predict_proba(region)]
    
    return data
